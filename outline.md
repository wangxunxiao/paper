
# 连享会 · 五一论文班

&emsp;

> &#x1F449; [最新版课纲 PDF](https://file.lianxh.cn/KC/lianxh_paper.pdf) | [课程主页](https://gitee.com/lianxh/paper)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&#x1F34E; &#x1F449; [助教名单公布了！](https://gitee.com/lianxh/paper/blob/master/%E5%8A%A9%E6%95%99%E5%85%A5%E9%80%89%E9%80%9A%E7%9F%A5.md)


&emsp;


## 1. 课程概览

> &#x1F4C5; **时间：** 2021 年 5 月 2-4 日 (9:00-12:00; 14:30-17:30, 半小时答疑)    
> &#x1F3A6; **方式：** 网络直播+回放   
> &#x1F511; **嘉宾：** 梁平汉(中山大学)；张川川 (浙江大学)；连玉君 (中山大学)   
> &#x1F353; **课程主页：** <https://gitee.com/lianxh/paper>  
> &#x1F449; **报名链接：** <http://junquan18903405450.mikecrm.com/6sFqyrI> 

&emsp;

### 授课嘉宾

**[梁平汉](http://sog.sysu.edu.cn/zh-hans/node/2827)**，中山大学政治与公共事务管理学院教授，博士生导师，中国公共管理研究中心研究员。主要研究博弈论、实验经济学、政府行为、发展经济学等。曾受邀在美国西北大学、美国加州大学、香港浸会大学等进行访问学者研究工作。在 Games and Economic Behavior、Journal of Comparative Economics、《经济研究》、《管理世界》、《经济学(季刊)》、《世界经济》等国内外权威核心学术期刊上发表论文三十余篇。曾获霍英东教育基金会高等院校青年教师奖。


**[张川川](https://person.zju.edu.cn/zhangchuanchuan)**， 浙江大学经济学院百人计划研究员、博士生导师，2013 年博士毕业于北京大学国家发展研究院，获经济学博士学位。研究方向为公共经济学、健康经济学和劳动经济学，研究兴趣包括老龄问题和社会保障、健康状况的决定因素和影响、文化观念与经济行为等。成果发表于 Demography, Health Economics, Journal of Population Economics, Journal of Comparative Economics,《中国社会科学》《经济研究》、《经济学季刊》、《世界经济》、《金融研究》等国内外期刊。

**[连玉君](https://lingnan.sysu.edu.cn/faculty/lianyujun)** ，中山大学岭南学院副教授，博士生导师。毕业于西安交通大学金禾经济研究，获经济学博士学位。研究方向为公司金融和金融计量，研究兴趣包括公司治理、现金持有、股权质押、投融资行为，成果见诸于 China Economic Review、《经济研究》、《管理世界》、《经济学(季刊)》、《金融研究》、《统计研究》、《世界经济》等期刊，主持国家自然科学基金 2 项。

&emsp;

### 课程导引

「杀手的身价决定于什么？」既非高深的武功，亦非锋利的兵器，而是「杀了谁」？

学术研究也是如此，论文的价值在很大程度上取决于你解决了什么问题，与之相伴的是一系列逻辑严密分析和论证，或曰研究设计。研读已经发表的论文通常只能看到作者研究工作的最终成果，而期间的思考、分析和打磨过程，则不得而知，而这恰恰是多数人渴望学习的。 

为此，本次专题课程中，三位嘉宾不仅仅是分享论文写作过程中「风光」的一面，也会倾诉「心酸」的体验。这次讲解的论文有半数都是嘉宾自己的论文，便源于这个考虑。

我们将通过精讲和重现多篇发表于 Top 期刊上的论文，以期有效提升诸位的研究设计和论文写作能力。课程中涉及的多数论文都会详细讲解实证研究的各个环节，包括：选题、研究设计、数据收集和处理、回归分析和结果解释等。在此基础上，进一步结合前期和后续文献，讨论其局限和可能的选题方向，以启发学员思考新的选题方向。课程中涉及多种计量方法的综合应用，包括：混合 OLS、固定效应模型、倍分法 (DID)、断点回归 (RDD)、安慰剂检验、蒙特卡洛模拟、内生性、稳健性检验等。


&emsp;

## 2. 专题介绍

三天的课程共包括六个专题，涉及 10 篇论文。其中，除 **[2] 梁平汉等 (2020, 世界经济)** 无法提供数据和程序外，其他 9 篇论文均提供重现数据和代码。

- 授课顺序  
  - T1-T2 &ensp; 梁平汉 &ensp; 5 月 2 日
  - T3-T4 &ensp; 张川川 &ensp; 5 月 3 日
  - T5-T6 &ensp; 连玉君 &ensp; 5 月 4 日
- &#x26F3; [浏览所有论文](https://www.jianguoyun.com/p/DVsJuh4QtKiFCBiPsOsD)

&emsp;

### T1 论文选题的路径：理论、文献与数据的视角    

本讲中，梁老师将根据切身经历，从理论、文献和数据三个视角分享实证论文的灵感出处、选题方向、研究设计与论文写作实现，并结合审稿过程讨论如何应对审稿人意见。本讲的重点在于为大家全程展示一篇论文的研究过程，涉及如下论文：
- **[1]** 于文超、殷华、梁平汉：税收征管，财政压力与企业融资约束，**中国工业经济**，2018 (1): 100-118. [-Link-](http://ciejournal.ajcass.org/Magazine/show/?id=52776), [-PDF1-](http://ciejournal.ajcass.org/UploadFile/Issue/32mtkjz3.pdf), [-PDF2-](https://www.jianguoyun.com/p/DRNZ4AcQtKiFCBj9susD), [-Replication-](http://ciejournal.ajcass.org/UploadFile/Issue/tsitp10t.zip)
- **[2]** 梁平汉，邹伟，胡超：时间就是金钱！退税无纸化改革、行政负担与企业出口，**世界经济**，2020 (10): 52-73. [-Link-](http://manu30.magtech.com.cn/sjjj/CN/abstract/abstract580.shtml), [-PDF-](https://www.jianguoyun.com/p/De9ifoIQtKiFCBiBrOsD)
- **[3]** 梁平汉、江鸿泽：金融可得性与互联网金融风险防范——基于网络传销案件的实证分析，**中国工业经济**，2020 (4): 116-134. [-Link-](http://ciejournal.ajcass.org/Magazine/show/?id=71836), [-PDF1-](http://ciejournal.ajcass.org/UploadFile/Issue/tqehdlo5.pdf), [-PDF2-](https://www.jianguoyun.com/p/Dat5uI4QtKiFCBj7susD), [-Replication-](http://ciejournal.ajcass.org/UploadFile/Issue/dqytqjv5.zip)。「政府数据的发掘与使用：以裁判文书为例」

&emsp; 

### T2 历史真实与想象真实：地理断点回归可靠吗？   

本讲拆解几篇基于「地理断点回归」的经典论文，以期了解该方法的基本思想、应用条件和陷阱：

- **[4]** Dell, Melissa, The Persistent Effects of Peru's Mining Mita,  **Econometrica**, 2010, 78 (6): 1863-1903. [-Link-Replication](https://scholar.harvard.edu/dell/publications/persistent-effects-perus-mining-mita), [-PDF-](https://www.jianguoyun.com/p/DQWBP7AQtKiFCBjlq-sD)
- **[5]** Dell, Melissa, Nathan Lane, and Pablo Querubin. The Historical State, Local Collective Action, and Economic Development in Vietnam. **Econometrica**, 2018, 86 (6): 2083-2121. [-Link-Replication](https://scholar.harvard.edu/dell/publications/state-capacity-local-governance-and-economic-development-vietnam), [-PDF-](https://www.jianguoyun.com/p/DWHp7L4QtKiFCBjZq-sD)
- **[6]** Becker, Sascha O., Mergele, Lukas, and Woessmann, Ludger. The Separation and Reunification of Germany: Rethinking a Natural Experiment Interpretation of the Enduring Effects of Communism. **Journal of Economic Perspectives**, 2020, 34 (2): 143-171. [-Link-](https://academic.microsoft.com/paper/3011328945/reference), [-Replication-](https://www.aeaweb.org/articles?id=10.1257/jep.34.2.143), [-PDF-](https://www.jianguoyun.com/p/DVx7krIQtKiFCBjTq-sD)

&emsp; 

### T3 讲好中国故事：Huang and Zhang (2020)    

第 3-4 讲的两篇文章都是张川川老师自己的论文。因此，在分享「方法」的同时，张老师也会「分享」这两篇文章的灵感出处、研究中遇到的障碍和解决方法。这两篇文章都是基于微观数据进行的，数据处理过程相对复杂。张老师将为大家分享如何处理多个来源的微观数据，以及如何发现数据中隐含的故事。

- **[7]** Huang, Wei and Chuanchuan Zhang, The Power of Social Pensions: Evidence from China’s New Rural Pension Scheme, ***American Economic Journal: Applied Economics***, forthcoming. [-PDF-](https://www.jianguoyun.com/p/DS5lM8EQtKiFCBj21tQD), [-Appendix-](https://www.jianguoyun.com/p/Det3jNYQtKiFCBj51tQD) 
- **提要**：本文研究了「中国新农村养老金计划（NRPS）」逐县推广这一政策的效果，发现在适龄人群中，养老金计划带来了更高的家庭收入和食品支出、更少的农活、更好的健康和更低的死亡率。此外，NRPS 将年龄不合格的成年人从农活转移到非农活，但并不显著影响他们的收入、支出或健康。


&emsp;

### T4 IV 估计：系数的解读和政策含义   

- **[8]** 张川川：“中等教育陷阱”？出口扩张、就业增长与个体教育决策，**经济研究**，2015 (12): 115-127. [-Link-](http://www.erj.cn/cn/lwInfo.aspx?m=20100921113738390893&n=20151103155323930383)，[-PDF-](https://www.jianguoyun.com/p/DVUlJxcQtKiFCBj11tQD)
- **内容提要：** 利用出口扩张引致的就业需求冲击和采用工具变量方法，本文从经验上分析了非农就业增长对个体教育投资的影响。研究发现，出口引致的就业增长导致适龄入学人口进入高中和大学的概率显著下降。非农就业人口占劳动年龄人口的比重增加 1 个百分点，导致 16-18 岁初中毕业生进入高中和 19-21 岁高中毕业生进入大学学习的概率分别下降 0.17% 和 0.26%。给定其他条件不变，这意味着 1990-2005 年间由出口扩张引致的就业增长使高中和大学入学率分别减少了 5.4 和 8.6 个百分点。分性别和城乡的考察显示：相对于女性，男性进入高中和大学的概率有更大幅度的下降；农村青年进入高中和大学学习的概率都随着就业增长出。
  


&emsp;

### T5 迁移  &rarr; 创新：如何基于经典论文选题？    

本主题讲解一篇公司金融领域的经典论文，这篇文章的引用率超过 2500 次，引申出了一系列新的话题，很多论文则采用该文的方法研究其他领域的问题。在学习本文的研究设计之余，我们会重点讨论如何从这些经典论文出发，寻求新的研究主题：是纵向深入、横向拼接/迁移，还是移花接木？

- **[9]** Faulkender, M., R. Wang, 2006, Corporate Financial Policy and the Value of Cash, ***Journal of Finance***, 61 (4): 1957-1990. [-PDF-](https://www.jianguoyun.com/p/DbP81TgQtKiFCBjWr-sD)，[-Link-](https://academic.microsoft.com/paper/2111681206), [-2,536 Citations-](https://academic.microsoft.com/paper/3121986562/citedby)
- **学习要点**
  - **讲解重点：** 如何从现有经典文献出发，探索新的研究主题
  - **计量方法：**  OLS、稳健性分析、衡量偏误、交叉项的应用和解释
  - **亮点：** 选题视角和研究设计值得借鉴，衡量偏误和模型设定等也处理的很妥当
- 扩展阅读 (泛读)：
  - Xu, N., Q. Chen, Y. Xu, K. Chan, **2016**, Political uncertainty and cash holdings: Evidence from china, **Journal of Corporate Finance**, 40: 276-295. [-Link-](https://academic.microsoft.com/paper/2513207166/reference), [-PDF-](https://www.jianguoyun.com/p/DYOiT4wQtKiFCBiOsOsD)
  - Bates, T., C.-H. Chang, J. Chi, **2018**, Why has the value of cash increased over time?, **Journal of Financial and Quantitative Analysis**, 53: 749 - 787. [-Link-](https://academic.microsoft.com/paper/2915016421/reference), [-PDF1-](https://sci-hub.ren/10.1017/S002210901700117X), [-PDF2-](https://www.jianguoyun.com/p/DbMmi3MQtKiFCBiNsOsD)
  - Aktas, N., C. Louca, D. Petmezas, **2019**, CEO overconfidence and the value of corporate cash holdings, **Journal of Corporate Finance**, 54: 85-106. [-Link-](https://academic.microsoft.com/paper/3122482262/reference), [-PDF1-](https://sci-hub.ren/10.1016/j.jcorpfin.2018.11.006), [-PDF2-](https://www.jianguoyun.com/p/DbgolXIQtKiFCBiMsOsD)
  - Deshmukh, S., A. M. Goel, K. M. Howe, **2021**, Do CEO beliefs affect corporate cash holdings?, **Journal of Corporate Finance**, 67. [-Link-](https://academic.microsoft.com/paper/3122580780/reference), [-PDF-](https://www.jianguoyun.com/p/DSCSnHIQtKiFCBiJsOsD)

&emsp;

### T6 户口值几钱？断点回归的研究设计如何做？      

虽然断点回归被视为一种比较干净的因果推断/政策评价方法，但其应用场景却往往因为我们的「想象力匮乏」而非常有限。本讲中介绍的这篇论文，采用主动出击的方式来获取数据，选择了一个大家都非常感兴趣的问题，并规范地呈现了 RDD 分析中的各种要点。 
- **[10]** Chen, Y., S. Shi, Y. Tang, **2019**, Valuing the urban hukou in china: Evidence from a regression discontinuity design for housing prices, **Journal of Development Economics**, 141. [-Link-](https://academic.microsoft.com/paper/2972667760/reference), [-PDF1-](https://sci-hub.ren/10.1016/j.jdeveco.2019.102381), [-PDF2-](https://www.jianguoyun.com/p/DXczSmoQtKiFCBiAs-sD), [Replication](https://data.mendeley.com/datasets/dtzfwv5vf2/1)
- **论文简介：** 2008年11月，济南市政府规定：在城区购买一套 90 平米以上的住房即可获得城市户口。该政策于2017取消。因此，作者从 [房天下](https://bj.fang.com/) 收集了2017年6月至2017年7月济南市上市住房交易数据，采用 RDD 来估算户口的市场价值。文中进行了多种稳健性检验、安慰剂检验、排他性检验、平滑性检验等，是一篇非常规范地应用 RDD 进行分析的实证论文。 

&emsp;

> &#x26F3; [浏览所有论文](https://www.jianguoyun.com/p/DVsJuh4QtKiFCBiPsOsD)

&emsp;

## 3. 听课指南

- **软件/课件：** Stata，提供全套 Stata 实操程序、数据和 dofiles (开课前一周发送)。建议使用 Stata 15.0 或更高版本。

- **听课软件**：本次课程可以在手机，ipad 以及 windows 系统的电脑上听课。
  - 手机/iPad：直接在应用商店搜索「**大黄蜂云课堂**」，安装即可
  - 电脑（windows）：<https://www.360dhf.cn/dhfplayer/>，将本网址在浏览器打开，下载「**大班课直播-学生端**」。

- **特别提示**：一个账号绑定一个设备，且听课电脑 **需要 windows 系统** ，请大家提前安排自己的听课设备。

&emsp;

## 4. 报名和缴费信息

- **主办方：** 太原君泉教育咨询有限公司
- **标准费用** (含报名费、材料费)：2200 元/人 (全价)
- **优惠方案**：
  - 三人及以上团购/连享会大课学员：2000元/人
  - 充值 [连享会会员](https://mp.weixin.qq.com/s/UTrDqy01VOT9NL2YsEYHqg)：八五折 1870元/人
  - **温馨提示：** 以上各项优惠不能叠加使用。
- **联系方式：**
  - 邮箱：[wjx004@sina.com](wjx004@sina.com)
  - 电话 (微信同号)： 王老师 18903405450 ; 李老师 ‭18636102467


&emsp;

### 报名链接

> **报名链接：** 
<http://junquan18903405450.mikecrm.com/6sFqyrI>

> 或 长按/扫描二维码报名：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/报名：五一论文班.png)



&emsp;

### 缴费方式

> **方式 1：对公转账**

- 户名：太原君泉教育咨询有限公司
- 账号：35117530000023891 (晋商银行股份有限公司太原南中环支行)
- **温馨提示：** 对公转账时，请务必提供「**汇款人姓名-单位**」信息，以便确认。

> **方式 2：微信扫码支付**

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/君泉收款码.png)

> **温馨提示：** 微信转账时，请务必在「添加备注」栏填写「**汇款人姓名-单位**」信息。


<div STYLE="page-break-after: always;"></div>

&emsp;

## 5. 诚聘助教 &#x26BD;

> ### 说明和要求

- **名额：** 6 名
- **任务：**
  - **A. 课前准备**：协助完成 2 篇推文，风格类似于 [lianxh.cn](https://www.lianxh.cn) ；
  - **B. 开课前答疑**：协助学员安装课件和软件，在微信群中回答一些常见问题；
  - **C. 上课期间答疑**：针对前一天学习的内容，在微信群中答疑 (8:00-9:00，19:00-22:00)；参见 [往期答疑](https://gitee.com/arlionn/PX/wikis/README.md?sort_id=3372005)。
  - Note: 下午 5:30-6:00 的课后答疑由主讲教师负责。
- **要求：** 热心、尽职，熟悉 Stata 的基本语法和常用命令，能对常见问题进行解答和记录。
- **特别说明：** 往期按期完成任务的助教自动获得本期助教资格，不必填写申请资料，直接联系连老师即可。
- **截止时间：** 2021 年 4 月 15 日 (将于 4 月 17 日公布遴选结果于连享会主页 [lianxh.cn](https://www.lianxh.cn) 或课程主页 <https://gitee.com/lianxh/paper>)

> **申请链接：** <https://www.wjx.top/vj/OtcwrLF.aspx> 

或扫码填写助教申请资料：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/助教招聘—2021论文班.png)

> **课程主页：** <https://gitee.com/lianxh/paper>


<div STYLE="page-break-after: always;"></div>

&emsp;

## 相关课程


&emsp; 

> &#x1F3A6; **2021 [效率分析专题](https://mp.weixin.qq.com/s/cRNqBnNrn92ODW92nnMhVg)**   
> &#x1F4C5; 2021 年 5.15-16；5.21-12     
> &#x1F511; 主讲：龚斌磊(浙江大学)；连玉君(中山大学)；张宁(暨南大学)       
> &#x1F353; **课程主页**：[https://gitee.com/lianxh/TE](https://gitee.com/lianxh/TE) ([微信版](https://mp.weixin.qq.com/s/cRNqBnNrn92ODW92nnMhVg))        

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh_TE_01_700.png)


&emsp;


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)
